#!py

def run():
  ret = {}
  pillar = __pillar__.get('consul',{})
  doc = pillar.get('kvstore',{})
  token = pillar.get('config',{}).get('acl_master_token',{})

  process(ret, token, doc, '', '')
  return ret

def process(ret, token, parent, path, subpath):
    for k, v in parent.items():
        if isinstance(v, dict):
            if len(parent.items()) > 1:
                subpath = path + '/' + k
                process(ret, token, v, subpath, subpath)
                subpath = ''
            else:
                path = path + '/' + k
                process(ret, token, v, path, subpath)
        else:
            if subpath:
                keypath = subpath +  '/' + k
            else:
                keypath = path +  '/' + k
            if token:
              ret['consul-kv-present-%s' % keypath[1:]] = {
                  'module.run': [
                      {'name': 'consul.put'},
                      {'consul_url': 'http://127.0.0.1:8500'},
                      {'token': str(token)},
                      {'key': keypath[1:]},
                      {'value': v}
                  ]
              }
            else:
              ret['consul-kv-present-%s' % keypath[1:]] = {
                  'module.run': [
                      {'name': 'consul.put'},
                      {'consul_url': 'http://127.0.0.1:8500'},
                      {'key': keypath[1:]},
                      {'value': v}
                  ]
              }
