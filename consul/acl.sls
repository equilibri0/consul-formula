{% from slspath+"/map.jinja" import consul with context %}

include:
  - {{ slspath }}.service

{% if consul.config.acl_agent_token is defined and consul.config.server %}
consul-agent-acl-bootstrap:
  http.wait_for_successful_query:
    - name: 'http://127.0.0.1:8500/v1/acl/create?token={{ consul.config.acl_master_token }}'
    - status: 200
    - request-interval: 5
    - method: "PUT"
    -  data: '{
      "Name": "Agent Token",
      "ID": "{{ consul.config.acl_agent_token }}",
      "Type": "client",
      "Rules": "node \"\" { policy = \"write\" } service \"\" { policy = \"read\" }"
      }'
  require:
    - service: consul-service
{% endif %}

{% if consul.get('acl_tokens', None) %}
{%- for name, acl in consul.acl_tokens.items() %}
consul-acl-present-{{name}}:
  module.run:
    - name: consul.acl_create
    - consul_url: http://localhost:8500
    - token: {{ consul.config.acl_master_token }}
    - kwargs: {
         name: "{{ acl['token_name'] }}",
         type: {{ acl['token_type'] }},
         id: {{ acl['token_id'] }},
         rules: {{ acl['token_rules'] }}
      }
{%- endfor %}
{% endif %}